// import { TaskStatusValues } from '../tasks/tasks.interfaces';
import { redisClient } from "../+configs/redis";
import { app, server } from "../index";
import supertest from "supertest";

const api = supertest(app);

describe("cards", () => {
  const validSignin = {
    card_number: "4242424242424242",
    cvv: "345",
    expiration_year: 2024,
    expiration_month: 2,
    email: "email1@gmail.com",
  };

  const invalidCardNumber = {
    card_number: "1111111111111111",
    cvv: "345",
    expiration_year: 2024,
    expiration_month: 2,
    email: "email1@gmail.com",
  };

  const invalidEmail = {
    card_number: "1111111111111111",
    cvv: "345",
    expiration_year: 2024,
    expiration_month: 2,
    email: "email1",
  };

  test("POST: /cards, ard Number", async () => {
    await api
      .post("/cards/signin")
      .send(invalidCardNumber)
      .expect(400)
      .expect("Content-Type", /json/);
  });

  test("POST: /cards, Invalid Invalid Email", async () => {
    await api
      .post("/cards/signin")
      .send(invalidEmail)
      .expect(400)
      .expect("Content-Type", /json/);
  });

  test("POST: /cards, Valid", async () => {
    await api
      .post("/cards/signin")
      .send(validSignin)
      .expect(201)
      .expect("Content-Type", /json/)
      .expect((res) => {
        expect(res.body.hasOwnProperty("token")).toEqual(true);
      });
  });

  test("GET: /cards/verify, Valid", async () => {
    const validResponse = await api.post("/cards/signin").send(validSignin);

    const validToken = validResponse.body.token;
    await api
      .get("/cards/verify")
      .set("Authorization", `Bearer ${validToken}`)
      .send()
      .expect(200)
      .expect("Content-Type", /json/)
      .expect((res) => {
        expect(res.body.card_number).toEqual(validSignin.card_number);
        expect(res.body.expiration_month).toEqual(validSignin.expiration_month);
        expect(res.body.expiration_year).toEqual(validSignin.expiration_year);
        expect(res.body.email).toEqual(validSignin.email);
      });
  });

  test("GET: /cards/verify, Valid", async () => {
    await api
      .get("/cards/verify")
      .set("Authorization", `Bearer invalid token`)
      .send()
      .expect(401)
      .expect("Content-Type", /json/)
      .expect((res) => {
        expect(res.body.hasOwnProperty("error")).toEqual(true);
        expect(res.body.error).toEqual("Unauthorized");
      });
  });
});

afterAll(async () => {
  redisClient.quit();
  server.close();
});
