import express, { Application } from "express";
import { Server } from "http";
import dotenv from "dotenv";
import { ApiInterface } from "./+base/base.interfaces";
import CardsRouter from "./cards/infraestructure/cards.routes";
import { redisClient } from "./+configs/redis";

class Api implements ApiInterface {
  app: Application;

  constructor() {
    this.app = express();
    this.configInit();
    this.configRoutes();
  }

  configInit() {
    dotenv.config();
    this.app.use(express.json());
  }

  configRoutes(): void {
    this.app.use("/cards", new CardsRouter().getRoutes());
  }

  async connectRedis() {
    return await redisClient.connect();
  }

  listen(): Server {
    const port = process.env.PORT || 3000;

    return this.app.listen(port, () => {
      console.log("server start!!");
    });
  }
}

const api = new Api();
api.connectRedis();
const server = api.listen();
const app = api.app;

export { app, server };
