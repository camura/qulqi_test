import { NextFunction, Request, Response } from "express";
import { TokensRepository } from "../../src/+tokens/tokens.respository";

export const validateTokenMiddleware = (tokenRepository: TokensRepository) => {
  return (_req: Request, _res: Response, _next: NextFunction) => {
    if (
      _req.headers.authorization &&
      _req.headers.authorization.startsWith("Bearer")
    ) {
      const token = _req.headers.authorization.split(" ")[1];
      const result = tokenRepository.verify(token);
      if (!result) {
        _res.status(401).json({ error: "Unauthorized" });
        return;
      }
      _res.locals.token = result;
      _next();
      return;
    }
    _res.status(401).json({ error: "Unauthorized" });
  };
};
