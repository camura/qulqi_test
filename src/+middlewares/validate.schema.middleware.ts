import { NextFunction, Request, Response } from "express";
import Joi from "joi";

export const validateSchemaMiddleware = (schema: Joi.ObjectSchema) => {
  return (_req: Request, _res: Response, _next: NextFunction) => {
    const { error } = schema.validate(_req.body);
    if (error) {
      _res.status(400).json({ error: error.details[0].message });
      return;
    }
    _next();
  };
};
