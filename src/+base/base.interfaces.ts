import { Application } from "express";
import { Server } from "http";

export interface ApiInterface {
  app: Application;
  configInit(): void;
  configRoutes(): void;
  connectRedis(): void;
  listen(): Server;
}
