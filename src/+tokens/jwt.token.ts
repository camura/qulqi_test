import { TokensRepository } from "./tokens.respository";
import jwt from "jsonwebtoken";

export class JwtToken implements TokensRepository {
  generate(value: string) {
    const token = jwt.sign(
      { id: value },
      process.env.SECRET_KEY || "secretkey",
      {
        expiresIn: 60,
      }
    );
    return token;
  }

  verify(token: string) {
    try {
      jwt.verify(token, process.env.SECRET_KEY || "secretkey");
      return token;
    } catch (error) {
      return null;
    }
  }
}
