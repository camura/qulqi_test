export interface TokensRepository {
  generate: (value: string) => string;
  verify: (token: string) => string | null;
}
