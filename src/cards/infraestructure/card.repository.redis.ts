import { redisClient } from "../../+configs/redis";
import { CardEntity } from "../domain/cards.entities";
import { CardsRepository } from "../domain/cards.repository";

export class CardRepositoryRedis implements CardsRepository {
  async saveToStore(card: CardEntity, token: string) {
    return await redisClient.set(token, JSON.stringify(card));
  }

  async getCard(token: string) {
    const result = await redisClient.get(token);
    if (!result) return null;
    return JSON.parse(result);
  }
}
