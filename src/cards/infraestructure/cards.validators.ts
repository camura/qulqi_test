import Joi from "joi";

export const cardsSigninSchema = Joi.object({
  card_number: Joi.string().creditCard().min(13).max(16).required(),
  cvv: Joi.string()
    .min(3)
    .max(4)
    .pattern(/^[0-9]+$/, "numbers")
    .required(),
  expiration_year: Joi.number().min(2023).max(2028).required(),
  expiration_month: Joi.number().min(1).max(12).required(),
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required()
    .min(5)
    .max(100),
});
