import { Request, Response } from "express";

import { CardEntity } from "../domain/cards.entities";
import { StoreCard } from "../application/storeCard";
import { CardRepositoryRedis } from "./card.repository.redis";
import { CardsRepository } from "../domain/cards.repository";
import { TokensRepository } from "../../+tokens/tokens.respository";
import { JwtToken } from "../../+tokens/jwt.token";
import { GetCard } from "../application/getCard";

export default class CardsController {
  cardsRepository: CardsRepository = new CardRepositoryRedis();
  tokenRepository: TokensRepository = new JwtToken();

  async signin(_req: Request, _res: Response) {
    try {
      const card: CardEntity = _req.body as CardEntity;
      const storeCard = new StoreCard(
        this.cardsRepository,
        this.tokenRepository
      );
      const token = await storeCard.save(card);
      _res.status(201).json({ token: token });
      return;
    } catch (error) {}
  }

  async verify(_req: Request, _res: Response) {
    const token = _res.locals.token;
    const getCard = new GetCard(this.cardsRepository);
    const result = await getCard.get(token);
    if (!result) {
      _res.status(400).json({ errors: "token invalid" });
      return;
    }
    _res.status(200).json(result);
  }
}
