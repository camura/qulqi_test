// import { tasksSchemaCreate, tasksSchemaUpdate } from "./task.validators";
import express, { Request, Response } from "express";
import CardsController from "./cards.controllers";
import { cardsSigninSchema } from "./cards.validators";
import { validateSchemaMiddleware } from "../../+middlewares/validate.schema.middleware";
import { validateTokenMiddleware } from "../../+middlewares/validate.token.middleware";
import { JwtToken } from "../../+tokens/jwt.token";

export default class CardsRouter {
  controller = new CardsController();
  router = express.Router();
  tokensRepository = new JwtToken();

  getRoutes() {
    this.router.post(
      "/signin",
      validateSchemaMiddleware(cardsSigninSchema),
      (_req: Request, _res: Response) => this.controller.signin(_req, _res)
    );
    this.router.get(
      "/verify",
      validateTokenMiddleware(this.tokensRepository),
      (_req: Request, _res: Response) => this.controller.verify(_req, _res)
    );
    return this.router;
  }
}
