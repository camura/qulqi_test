import { CardResponseEntity } from "../domain/cards.entities";
import { CardsRepository } from "../domain/cards.repository";

export class GetCard {
  constructor(private cardRepository: CardsRepository) {}
  async get(token: string): Promise<CardResponseEntity | null> {
    const result = await this.cardRepository.getCard(token);
    if (!result) return null;
    const { cvv: _, ...card } = result;
    return card;
  }
}
