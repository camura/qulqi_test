import { TokensRepository } from "../../+tokens/tokens.respository";
import { CardEntity } from "../domain/cards.entities";
import { CardsRepository } from "../domain/cards.repository";

export class StoreCard {
  constructor(
    private cardRepository: CardsRepository,
    private tokensRepository: TokensRepository
  ) {}
  async save(card: CardEntity) {
    const token = this.generateToken(card.card_number);
    this.cardRepository.saveToStore(card, token);
    return token;
  }

  generateToken(value: string) {
    return this.tokensRepository.generate(value);
  }
}
