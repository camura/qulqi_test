export interface CardEntity {
  card_number: string;
  cvv: string;
  expiration_year: number;
  expiration_month: number;
  email: string;
}

export type CardResponseEntity = Omit<CardEntity, "cvv">;
