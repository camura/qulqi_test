import { CardEntity } from "./cards.entities";

export interface CardsRepository {
  saveToStore: (card: CardEntity, token: string) => Promise<string | null>;
  getCard: (token: string) => Promise<CardEntity | null>;
}
