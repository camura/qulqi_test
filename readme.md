### Installar

**Descargar el repositorio y ejecutar:**

- npm install

**Crear un .env con las siguientes variables:**

- REDIS_HOST: servidor local de redis
- REDIS_PORT: puerto, por defecto 6379
- SECRET_KEY: secret para generar tokens
- PORT: puerto donde se ejecuta el servidor

---

### Comandos

- npm run tsc: genera el build
- npm run test: ejecuta test
- npm run dev: inicia en local

---

### Endpoints

- **/cards/signin**: Guarda la tarjeta y retorna un token de autorizacion asociado a la tarjeta.
- **/cards/verify**: Obtienes los datos de la tarjeta asociada al token que se envia

---

### Ejemplos

**/card/signin**
{
"card_number": "4242424242424242",
"cvv": "456",
"expiration_year": 2024,
"expiration_month": 2
}

---

### condideración

Debe tener instalado redis en local
